// Node JS REview
/* 
    Benefits
        -Performance
            -Optimized for web applications
        -Familiarity
            -"Same old" Javascript
        -Access to Node Package Manager (NPM)
            - World's largest registry of packages

*/
/* 
    Limitations
        - No direct support for common web development
            - Handling HTTP verbs (GET, POST, DELETE, etc.,)
            - Handling request at different URL paths (routing)
            - Serving of static files
        - Node.JS for web development may get tedious

*/

// Express.JS
/* 
    Web Framework
        - A set of components designed to simplify the web dev process
            - Convention over configuration
        - Allows devs to focus on the business logic of their app
        - Comes in two forms: opinionated and unopinionated

*/
/* 
    Opinionated Web Framework
        - Dictates how it should be used by the dev
        - Speeds up the startup process of app development
        - Enforces best practices for the framework's use case
        - Lack of flexibility could be a drawback when app's needs are not aligned with the framework's assumptions
    
    Unopinionated Web Framework
        - Dev dictates how to use the framework
        - Offers flexibility to create an application unbound by any use case
        - No "right way" of structuring an application
        - Abundance of options may be overwhelming

*/
/* 
    Advantages (over plain node.js):
        - Simplicity makes it easy to learn
        -

*/

// npm init or npm init -y
// npm install express or npm install@<version number> or npm install express mongoose
// touch .gitignore

// npm install mongoose
// npm install cors
// npm install bcrypt - for password encryption
// npm install jsonwebtoken - allow us to gain access to other method













/* | ( . ) ( . ) |
/ ( . ) ( . ) \
( ( . ) ( . ) )
| ( . ) |
 */