const express = require("express");
/* const { response } = require("express");
const { request } = require("http"); */

const app = express();

const port = 3001;

app.use(express.json())

app.use(express.urlencoded({extended: true}));

let users = [];
    /* {
        "userName": "johndoe",
        "password": "johndoe123"
    }; */

app.get('/', (request, response) => {
	response.send("Welcome to the home page")
});

app.get('/home', (request, response) => {
	response.send("Welcome to the home page")
});


app.post("/register", (request, response) => {
    if(request.body.username !== "" && request.body.password !== ""){
        users.push(request.body);
        response.send(`User ${request.body.username} successfully registered!`);
        console.log(request.body);
    } else {
        response.send("Please input BOTH username and password")
    }
});

app.get("/users", (request, response) => {
    if(users.length !== 0){
        response.send(users);
    } 
    else {
        response.send("There are no users registered.")
    }
});

/* app.get('/users', (request, response) => {
    response.send(users)
}); */

/* app.delete('/delete-user', (request, response) => {
    response.send(`User ${users.userName} has been deleted.`)
}); */

app.delete("/delete-user", (request, response) => {
    
    let message;

    if (users.length != 0) {

        for (let i = 0; i < users.length; i++) {
            if (request.body.userName == users[i].userName) {
                users.splice(users[i], 1);
                message = `User ${request.body.userName} has been deleted.`;
                break;
            }
            else {
                message = "User does not exist.";
            }
        }
    }
    else {
        message = "User does not exist. No more users to delete.";
    }          
        
    
    response.send(message);
});

app.listen(port, () => console.log(`Server running at ${port}`));